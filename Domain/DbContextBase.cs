﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Domain
{
    public class DbContextBase : DbContext
    {
        public DbContextBase(DbContextOptions options) : base(options)
        {

        }
    }
}
